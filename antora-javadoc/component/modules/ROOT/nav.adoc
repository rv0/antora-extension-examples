* xref:index.adoc[Home Page]
//refs to .adoc until https://gitlab.com/antora/antora/-/issues/596 is solved
* xref:javadoc:overview-summary.html#[Geronimo Transaction Manager Javadoc]
* xref:felix:scr/overview-summary.html#[Felix Declarative Services Javadoc]
* xref:tomee:index.html#[TomEE Javadoc]
** xref:tomee:jakarta/servlet/package-summary.html#[Servlet API]
** xref:tomee:org/apache/tomee/package-summary.html#[TomEE]
** xref:tomee:org/eclipse/microprofile/opentracing/package-summary.html#[OpenTracing]
